angular.module('barcodeApp.services', ['ionic', 'ngCordova'])

.service('PopupService', function($ionicPopup, ItensService) {
	this.showPopup = function(barcode) {
      this.data = {}
    
      var promptPopup = $ionicPopup.prompt({
         title: 'Adicionar Novo Item',
         subTitle: 'Barcode: ' + barcode,
         template: 'Nome',
         inputType: 'text',
         inputPlaceholder: ' Nome'
      });
        
      promptPopup.then(function(res) {
        if(res) {
            ItensService.insert(res, barcode);
            ItensService.all();
        }
      });  
   };
})

.service('PopupExistService', function($ionicPopup, ItensService) {
    this.showAlert = function() {
    
      var alertPopup = $ionicPopup.alert({
         title: 'Alerta',
         template: 'Código de barras já cadastrado.'
      });
        
      alertPopup.then(function(res) {
        
      });  
   };
})

.service('PopupEditService', function($ionicPopup, ItensService) {
    this.showEditPopup = function(item) {
      this.data = {}
    
      var promptPopup = $ionicPopup.prompt({
         title: 'Editar Item',
         subTitle: 'Barcode: ' + item.barcode,
         template: 'Nome',
         inputType: 'text',
         inputPlaceholder: ' Nome'
      });
        
      promptPopup.then(function(res) {
        if(res) {
            ItensService.update(item.id, res);
            ItensService.all();
        }
      });  
   };
})

.service('PopupConfirmService', function($ionicPopup, ItensService) {
    this.showConfirmPopup = function(item) {
      this.data = {}

      var confirmPopup = $ionicPopup.confirm({
         title: 'Deletar Item: ' + item.name,
         template: 'Você tem certeza?'
      });

      confirmPopup.then(function(res) {
         if(res) {
            ItensService.delete(item.id);
            ItensService.all();
         }
      });
   };
})

.service('ItensService', function($rootScope, $cordovaSQLite) {
	this.insert = function(name, barcode) {
        var query = "INSERT INTO item (name, barcode) VALUES (?,?)";
        $cordovaSQLite.execute(db, query, [name, barcode]).then(function(result) {
            console.log("INSERT ID -> " + result.insertId);
        }, function (err) {
            console.error(err);
        });
    };
    
    this.select = function(barcode, callback) {
        var query = "SELECT * FROM item WHERE barcode = '"+barcode+"'";
        $cordovaSQLite.execute(db, query, []).then(function(result) {
            if(result.rows.length > 0) {
                callback(true);
            } else {
                callback(false);
            }
        }, function (err) {
            console.error(err);
        });
    };

    this.delete = function(id) {
        var query = "DELETE FROM item WHERE id = '"+id+"'";
        $cordovaSQLite.execute(db, query, []).then(function(result) {
            if(result.rows.length > 0) {
                console.log("DELETED -> " + result.rows.item(0).name + " " + result.rows.item(0).barcode);
            } 
        }, function (err) {
            console.error(err);
        });
    };

    this.update = function(id, name) {
        var query = "UPDATE item SET name = ? WHERE id = ? ";
        $cordovaSQLite.execute(db, query, [name, id]).then(function(result) {
            if(result.rows.length > 0) {
                console.log("UPDATED -> " + result.rows.item(0).name + " " + result.rows.item(0).barcode);
            }
        }, function (err) {
            console.error(err);
        });
    };

    this.all = function() {
        var query = "SELECT * FROM item";
        $cordovaSQLite.execute(db, query).then(function(result) {
        	$rootScope.listItens = [];
            if(result.rows.length > 0) {
            	var len = result.rows.length;
                for (var i=0; i<len; i++){
                    var item = {
                        'id': result.rows.item(i).id,
                        'name': result.rows.item(i).name,
                        'barcode': result.rows.item(i).barcode
                    };
                    $rootScope.listItens.push(item);
                }
            } else {
                console.log("No results found");
            }
        }, function (err) {
            console.error(err);
        });
    };
});