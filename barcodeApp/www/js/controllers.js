angular.module('barcodeApp.controllers', ['ionic', 'ngCordova'])

.controller("ScanController", function($scope, $rootScope, $cordovaBarcodeScanner, PopupService, PopupExistService, ItensService) {
    $scope.scanBarcode = function() {
      $cordovaBarcodeScanner.scan().then(function(imageData) {
        if(imageData.text.length > 0) {

            ItensService.select(imageData.text, function(result) {

                if (!result) {
                    PopupService.showPopup(imageData.text);
                } else {
                    PopupExistService.showAlert();
                }
            });
        }
     
        }, function(error) {
          $rootScope.scanResult = error;
        });
    };
})

.controller("ItensController", function($scope, $rootScope, $cordovaSQLite, ItensService) {

    $scope.insert = function (name, barcode) {
   		ItensService.insert(name, barcode);
   	};
 
    $scope.select = function(barcode) {
        ItensService.select(barcode);
    };

    $scope.delete = function(id) {
        ItensService.delete(id);
    };

    $scope.update = function(id, name) {
        ItensService.update(id, name);
    };

    $scope.all = function() {
        ItensService.all();
    };

    $scope.destroy = function() {
        var query = "DROP TABLE item";
        $cordovaSQLite.execute(db, query)
    };
})

.controller('PopupController', function($scope, $rootScope, $ionicPopup, PopupService) {
	$scope.popup = function () {
   		PopupService.showPopup();
   	};
})

.controller('ListController', function($scope, $rootScope, ItensService, PopupEditService, PopupConfirmService) {
  
  $scope.listCanSwipe = true
  
  $scope.edit = function(item) {
    PopupEditService.showEditPopup(item);
  };
  $scope.delete = function(item) {
    PopupConfirmService.showConfirmPopup(item);
  };
});