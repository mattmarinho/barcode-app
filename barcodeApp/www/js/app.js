angular.module('barcodeApp', ['barcodeApp.controllers', 'barcodeApp.services', 'ionic', 'ngCordova'])

.run(function($ionicPlatform, $cordovaSQLite, ItensService) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    if(window.cordova) {
      db = $cordovaSQLite.openDB({name: "bcApp.db", location: 'default'});
    } else {
      db = window.openDatabase("bcApp.db", "1.0", "barcodeApp", -1);
    }

    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS item (id integer primary key, name text, barcode text)");

    ItensService.all();
  });
});